///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   1 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };

class Animal {

public:
   Animal();
   ~Animal();
   
   enum Gender gender;
	   string      species;

	   virtual const string speak() = 0;
	
   	void printInfo();
	
	   string colorName  (enum Color color);
	   string genderName (enum Gender gender);
   
   //Random attribute generator
   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const bool getRandomBool();
   static const float getRandomWeight( const float from, const float to);
   static const string getRandomName();
};

} // namespace animalfarm
