///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   1 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>
#include <array>
#include <list>

#include "animal.hpp"
#include "animalF.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;

   //Array with 25 random animals
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for(int i = 0; i < 25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   //Print headers of the Array
   cout << endl << "Array of Animals" << endl;
   cout << "  Is it empty: " << ( animalArray.empty() ? "true" : "false" ) << endl;
   cout << "  Number of elements: " << animalArray.size() << endl;
   cout << "  Max size: " << animalArray.max_size() << endl;

   //Have each animal speak
   for(Animal* animal : animalArray){
      if(animal != NULL){
      cout << animal->speak() << endl;
      }
   }

   //Cleanup and delete
   for(Animal* animal : animalArray){
      if(animal != NULL){
         delete animal;
      }
   }

   //List with 25 random animals
   list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
   
      animalList.push_front(AnimalFactory::getRandomAnimal());

   }

   //Header for list
   cout << endl << "List of Animals" << endl;
   cout << "  Is it empty: " << ( animalList.empty() ? "true" : "false" ) << endl;
   cout << "  Number of elements: " << animalList.size() << endl;
   cout << "  Max size: " << animalList.max_size() << endl;


   //each animal in the list speaks
   for(Animal* animal : animalList){
      cout << animal->speak() << endl;
   }

   //clean up
   for(Animal* animal : animalList){
      delete animal;   
   }
   
   return 0;

} //end main

