///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalF.hpp
/// @version 1.0
///
/// Animal Factory
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

using namespace std;

namespace animalfarm{

class AnimalFactory { //: public Animal{
   public:
      static Animal* getRandomAnimal();
};
}

